let temp = []

const loadtemp = function(){
    const tempJSON = sessionStorage.getItem('temp')
    
    if(tempJSON !== null){
        return JSON.parse(tempJSON)
    } else {
        return []
    }
}

const savetemp = function(){
    sessionStorage.setItem('temp', JSON.stringify(temp))
}

//expose orders from module
const gettemp = () => temp

const criatemp = (select, hamb, precoProduto, tx, bairro) =>{
    
    temp.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        taxa: tx,
        nomeDoBairro: bairro
    })
    savetemp()
}

const removetemp = (item) => {
    temp.splice(item, 1)
    savetemp()
}

const apagatemp = () => sessionStorage.removeItem('temp')

temp = loadtemp()

export { gettemp, criatemp, savetemp, removetemp, apagatemp}