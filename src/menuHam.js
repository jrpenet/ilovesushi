import {criaPedidos} from './pedidos'
import { criatemp} from './temp'

//gera o menu na tela

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Uramaki Filadelfia',
    descricao: 'Arroz, cream cheese, cebolinho e salmão ou kani ou camarão ou skin. Valor por peça.',
    preco: 2,
    foto: './images/uramaki.jpeg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Eby Joy',
    descricao: 'Arroz, salmão, cream cheese, cebolinho e camarão. Valor por peça.',
    preco: 2.2,
    foto: './images/ebyjoy.jpg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Joy Joy',
    descricao: 'Arroz, salmão, cream cheese, cebolinho e pasta de salmão. Valor por peça.',
    preco: 2.2,
    foto: './images/joyjoy.jpeg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Carioca',
    descricao: 'Arroz, salmão, cream cheese e cebolinho. Valor por peça.',
    preco: 1.8,
    foto: './images/carioca.jpeg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Joy Goiabada',
    descricao: 'Sabor goiabada. Valor por peça.',
    preco: 1.5,
    foto: './images/joygoiabada.jpeg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Dragon',
    descricao: 'Arroz, salmão, cream cheese e cebolinho. Valor por peça.',
    preco: 2.3,
    foto: './images/dragon.jpeg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Hossomaki',
    descricao: 'Arroz, salmão e alga por fora. Valor por peça. Escolha os sabores: Salmão, Kani, Camarão.',
    preco: 2,
    foto: './images/hossomaki.jpeg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Sashimi (sashimi unidade)',
    descricao: 'Escolha os sabores: Salmão, Kani, Camarão, Polvo',
    preco: 3,
    foto: './images/sashimi_319387313-1000x667.jpg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Manta de Salmão',
    descricao: 'Manta de salmão com cream cheese e camarão empanado',
    preco: 45,
    foto: './images/manta.jpg',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sushi Hot - Ilovesushi',
    descricao: '',
    preco: 24.99,
    foto: './images/shotIlove.jpg',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Niguiri',
    descricao: 'Salmão por cima e bolinho de arroz. Valor por peça.',
    preco: 1.9,
    foto: './images/niguiri.jpeg',
    tipo: 'hot'
}, {
    nomeHamburguer: 'Sunomono',
    descricao: 'Pepino, salmão e gergelim. Escolha os sabores: Salmão, Kani, Camarão, Misto.',
    preco: 18.9,
    foto: './images/sunomono.jpeg',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Temakis',
    descricao: 'Sabores: Salmão, Camarão, Kani, Misto, Skin e Carioca.',
    preco: 0,
    foto: './images/temakiskin.jpg',
    tipo: 'temaki'
}, {
    nomeHamburguer: 'Combo 01 - (36 peças)',
    descricao: '12 Cariocas Salmão, 12 Cariocas Kani, 12 Cariocas Camarão',
    preco: 29.99,
    foto: './images/socariocas.jpeg',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 02 - (35 peças)',
    descricao: '10 Cariocas Salmão, 05 Hossomaki Salmão, 05 Hossomaki Kani, 05 Uramaki Salmão, 05 Uramaki Kani, 05 Niguiris Variados ',
    preco: 40,
    foto: './images/uber.JPG',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 03 - (50 peças)',
    descricao: '10 Cariocas Salmão, 05 Uramaki Salmão, 05 Uramaki Kani, 05 Hossomaki Salmão, 05 Hossomaki Kani, 10 Niguiri Variado, 10 hot massa Haromaki',
    preco: 60,
    foto: './images/uber.JPG',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 04 - (60 peças)',
    descricao: '10 Cariocas Kani, 10 Cariocas Salmão, 10 Uramaki Salmão, 10 Uramaki Kani, 10 Niguiri Variado, 10 Canapés Variados',
    preco: 75,
    foto: './images/uber.JPG',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 05 - (50 peças)',
    descricao: '10 canapés camarão, 10 canapés salmão, 12 cariocas salmão, 10 uramaki salmão e 5 kani queijo',
    preco: 65,
    foto: './images/uber.JPG',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 06 - (15 peças)',
    descricao: 'Camarão especial do chefe, recheado com cream cheese empanado e frito',
    preco: 35.9,
    foto: './images/uber.JPG',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 09 - (20 peças)',
    descricao: 'Hot especial de massa Haromaki Salmão, Cream Cheese e Cebolinha',
    preco: 30,
    foto: './images/combo9.JPG',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 11 - Joy Joy e Ebi Joy (20 peças)',
    descricao: '10 Joy Joy e 10 Eby Joy',
    preco: 40,
    foto: './images/joy-joy.jpg',
    tipo: 'combos'
}, {
    nomeHamburguer: 'Combo 12 - (20 peças)',
    descricao: '10 Madalena especial Camarão Empanado, Salmão e Cream Cheese; 10 Kani Queijo empanado e frito especial',
    preco: 40,
    foto: './images/combo12.JPG',
    tipo: 'combos'
}]

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const opt4 = document.createElement('option')
    const opt5 = document.createElement('option')
    const opt6 = document.createElement('option')
    const opt7 = document.createElement('option')
    const opt8 = document.createElement('option')
    const opt9 = document.createElement('option')
    const opt10 = document.createElement('option')
    const opt11 = document.createElement('option')
    const opt12 = document.createElement('option')
    const opt13 = document.createElement('option')
    const opt14 = document.createElement('option')
    const opt15 = document.createElement('option')
    const opt16 = document.createElement('option')
    const opt17 = document.createElement('option')
    const opt18 = document.createElement('option')
    const opt19 = document.createElement('option')
    const opt20 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    opt4.textContent = 4
    opt4.setAttribute('value', '4')
    opt5.textContent = 5
    opt5.setAttribute('value', '5')
    opt6.textContent = 6
    opt6.setAttribute('value', '6')
    opt7.textContent = 7
    opt7.setAttribute('value', '7')
    opt8.textContent = 8
    opt8.setAttribute('value', '8')
    opt9.textContent = 9
    opt9.setAttribute('value', '9')
    opt10.textContent = 10
    opt10.setAttribute('value', '10')
    opt11.textContent = 11
    opt11.setAttribute('value', '11')
    opt12.textContent = 12
    opt12.setAttribute('value', '12')
    opt13.textContent = 13
    opt13.setAttribute('value', '13')
    opt14.textContent = 14
    opt14.setAttribute('value', '14')
    opt15.textContent = 15
    opt15.setAttribute('value', '15')
    opt16.textContent = 16
    opt16.setAttribute('value', '16')
    opt17.textContent = 17
    opt17.setAttribute('value', '17')
    opt18.textContent = 18
    opt18.setAttribute('value', '18')
    opt19.textContent = 19 
    opt19.setAttribute('value', '19')
    opt20.textContent = 20
    opt20.setAttribute('value', '20')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    if(lanche.tipo === 'temaki' || lanche.tipo === 'sunomono' || lanche.tipo === 'combos') {
        select.appendChild(opt)
        select.appendChild(opt2)
        select.appendChild(opt3)
        select.appendChild(opt4)
    }
    select.appendChild(opt5)
    if(lanche.tipo === 'hot') {
        select.appendChild(opt6)
        select.appendChild(opt7)
        select.appendChild(opt8)
        select.appendChild(opt9)
        select.appendChild(opt10)
        select.appendChild(opt11)
        select.appendChild(opt12)
        select.appendChild(opt13)
        select.appendChild(opt14)
        select.appendChild(opt15)
        select.appendChild(opt16)
        select.appendChild(opt17)
        select.appendChild(opt18)
        select.appendChild(opt19)
        select.appendChild(opt20)
    }

    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        if(prodt === 'Uramaki Filadelfia' || prodt === 'Temaki Frito' || prodt === 'Sushi Hot - Ilovesushi' || prodt === 'Sunomono' || prodt === 'Sashimi (sashimi unidade)' || prodt === 'Hossomaki' || prodt === 'Temakis' ){
            criatemp(qt, prodt, price)
            location.assign('./conftemp.html')
        }else{
            criaPedidos(qt, prodt, price)
            location.assign('./beverage.html')
        }       
    })

    imagem.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        if(prodt === 'Uramaki Filadelfia' || prodt === 'Temaki Frito' || prodt === 'Sushi Hot - Ilovesushi' || prodt === 'Sunomono' || prodt === 'Sashimi (sashimi unidade)' || prodt === 'Hossomaki' || prodt === 'Temakis'){
            criatemp(qt, prodt, price)
            location.assign('./conftemp.html')
        }else{
            criaPedidos(qt, prodt, price)
            location.assign('./beverage.html')
        }       
    })

    return divMain
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo === 'hot' || x.tipo == 'sunomono').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable').appendChild(hrEl)
        document.querySelector('#docTable').appendChild(addElements(lanche))
        
    })
}

const addCardapioTemakis = () => {
    cardapio.filter((x) => x.tipo === 'temaki').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable2').appendChild(hrEl)
        document.querySelector('#docTable2').appendChild(addElements(lanche))
    })
}

const addCombos = () => {
    cardapio.filter((x) => x.tipo == 'combos').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable3').appendChild(hrEl)
        document.querySelector('#docTable3').appendChild(addElements(lanche))
    })
}

//sabores

//sabores está aqui temporariamente devido a qtd de itens disponiveis no cardapio, se mt grande mudar para page alone

const sabores = [{
    nomeMolho: 'Skin',
    codigo: 'skin',
    preco: 0,
    tipo: 'sabor'
},{
    nomeMolho: 'Camarão',
    codigo: 'camarao',
    preco: 0,
    tipo: 'sabor'
},{
    nomeMolho: 'Kani',
    codigo: 'kani',
    preco: 0,
    tipo: 'sabor'
},{
    nomeMolho: 'Salmão',
    codigo: 'salmao',
    preco: 0,
    tipo: 'sabor'
},{
    nomeMolho: 'Misto',
    codigo: 'misto',
    preco: 0,
    tipo: 'sabor'
},{
    nomeMolho: 'Polvo',
    codigo: 'polvo',
    preco: 0,
    tipo: 'sabor'
}]

const addElementsSabores = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('span')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    //const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('input')
    const lbl = document.createElement('label')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeMolho
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    
    //btn.textContent = 'PEDIR'
    if(lanche.nomeMolho === 'Skin'){
        btn.setAttribute('checked', '')
    }

    btn.setAttribute('type', 'radio')
    btn.setAttribute('name', 'optMolhos')
    btn.setAttribute('id', `chkMolho${lanche.codigo}`)
    btn.setAttribute('value', `${lanche.nomeMolho}`)
    
    divMain.appendChild(lbl)
    lbl.appendChild(btn)
    lbl.appendChild(titulo)

    return divMain
}

const addCardapioMolho = () => {
    sabores.forEach((lanche) => {
        document.querySelector('#molhosRecheios').appendChild(addElementsSabores(lanche))
    })
}


export {addCardapio, addCardapioTemakis, addElements, addCardapioMolho, addCombos}