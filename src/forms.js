import {insereClientes, getClientes} from './cliente'
import {getPedidos} from './pedidos'

//refresh automatico pra evitar bugs

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

if(getClientes().length > 0) {
   location.assign('./confcli.html')
}

if(getPedidos().map((x)=> x.taxa).includes('taxa')){
   
}else{
   location.assign('./txentrega.html')
}

document.querySelector('#confirmarPedido').addEventListener('click', (e) => {
   e.preventDefault()
   const nome = document.querySelector('#nome').value
   const endereco = document.querySelector('#endereco').value
   const num = document.querySelector('#numero').value
   const cep = document.querySelector('#cep').value
   const cel = document.querySelector('#celular').value
   const ref = document.querySelector('#pto_referencia').value
   const pgt = document.querySelector('#pagaEM').value
   const troco = document.querySelector('#troco').value
   const obs = document.querySelector('#observacao').value

   if(nome === '' || endereco === '' || cel === '' || cep === '' || num === ''){ //bairro === '' || 
      alert('Os campos nome, cep, endereço/nr e celular são obrigatórios')
     } else {
        insereClientes(nome, endereco, num, cep, cel, ref, pgt, troco, obs)
        location.assign('./enviapedidos.html')
   }

})

const insereBairro = () => {
   const bairroNoForm = getPedidos().map((z) => z.nomeDoBairro).join('')
   document.querySelector('#bairro').textContent = bairroNoForm
}

insereBairro()

const pegaoCep = document.querySelector('#cep')
pegaoCep.addEventListener('input', () => {
   if(pegaoCep.value.length > 7) {
      const url = 'https://viacep.com.br/ws/' + pegaoCep.value.replace('-', '') +  '/json/'
      fetch(url)
         .then((r) => r.json())
         .then(data => {
            if(data.logradouro == undefined || data.bairro == undefined || data.localidade  == undefined || data.uf  == undefined){
               alert('Endereço inválido')
               const rua = document.querySelector('#endereco')
               rua.value = ''
            }else{
               const rua = document.querySelector('#endereco')
               rua.value = data.logradouro
            }
      })
   }
   
       
})

const formadepgt = document.querySelector('#pagaEM')
formadepgt.addEventListener('change', (e) => {
   if(e.target.value == 'Pix'){
      alert('Necessário enviar o comprovante após nos enviar o pedido, a chave do Pix é o CPF número: 05315303454, Nome: Ibison Correia Laurentino')
   }else if(e.target.value == 'cartão de crédito' || e.target.value == 'cartão de débito'){
      alert('O valor aumenta R$ 1.00 se pagar no crédito ou débito')
   }   
})