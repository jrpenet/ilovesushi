import {getClientes, mudaDados} from './cliente'
import {getPedidos} from './pedidos'

const nomeBairro = (nome) => nome.nomeDoBairro

const nome = document.querySelector('#nomeNoPreenchimento')
const endereco = document.querySelector('#endNoPreenchimento')
const bairro = document.querySelector('#bairroNoPreenchimento')
const ref = document.querySelector('#referenciaNoPreenchimento')
const cel = document.querySelector('#foneNoPreenchimento')
const nome2 = document.querySelector('#nomeNoPreenchimento2')

nome.textContent = getClientes()[0].toUpperCase()
endereco.textContent = getClientes()[1]
bairro.textContent = getPedidos().filter(nomeBairro).map((x) => x.nomeDoBairro)
cel.textContent = getClientes()[4]
ref.textContent = getClientes()[5]
nome2.textContent = getClientes()[0].toUpperCase()

const rmvcli = document.querySelector('#mudacli')
rmvcli.addEventListener('click', (e) => {
    localStorage.clear()
})

document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./confirma.html')
})

document.querySelector('#confirmarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    
    const pgt = document.querySelector('#pagaEM').value
    const troco = document.querySelector('#troco').value
    const obs = document.querySelector('#observacao').value

    mudaDados(pgt, troco, obs)
    location.assign('./enviapedidos.html')

})

const formadepgt = document.querySelector('#pagaEM')
formadepgt.addEventListener('change', (e) => {
    if(e.target.value == 'Pix'){
        alert('Necessário enviar o comprovante após nos enviar o pedido, a chave do Pix é o CPF número: 05315303454, Nome: Ibison Correia Laurentino')
    }else if(e.target.value == 'cartão de crédito' || e.target.value == 'cartão de débito'){
        alert('O valor aumenta R$ 1.00 se pagar no crédito ou débito')
    }   
})