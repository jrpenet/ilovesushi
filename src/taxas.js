import {criaPedidos, getPedidos} from './pedidos'

const pulaTaxa = () => {
    if(getPedidos().map((x) => x.taxa).includes('taxa')){
        location.assign('./confirma.html')
    }
}

const addTblEl = function (){
    const tblEl = document.createElement('table')
    document.querySelector('#txBairro').appendChild(tblEl)
    
    //header da tabela
    
    // const qtyEl = document.createElement('th')
    // qtyEl.textContent = ''
    // document.querySelector('table').appendChild(qtyEl)
    
    const district = document.createElement('th')
    district.textContent = 'BAIRRO'
    document.querySelector('table').appendChild(district)
    
    const priceEl = document.createElement('th')
    priceEl.textContent = 'TAXA DE ENTREGA'
    document.querySelector('table').appendChild(priceEl)
    
    const solicitaTx = document.createElement('th')
    solicitaTx.textContent = 'PEDIR'
    document.querySelector('table').appendChild(solicitaTx)
}

//gera o menu na tabela

const bairrosLista = [{
    nomeBairro: 'Retirar na loja',
    valor: 0,
    taxa: 'taxa',
    formBairro: 'Retirar na loja'
},{
    nomeBairro: 'UR-1',
    valor: 4,
    taxa: 'taxa',
    formBairro: 'UR-1'
},{
    nomeBairro: 'UR-2',
    valor: 6,
    taxa: 'taxa',
    formBairro: 'UR-2'
},{
    nomeBairro: 'UR-3',
    valor: 6,
    taxa: 'taxa',
    formBairro: 'UR-3'
},{
    nomeBairro: 'UR-4',
    valor: 5,
    taxa: 'taxa',
    formBairro: 'UR-4'
},{
    nomeBairro: 'UR-5',
    valor: 5,
    taxa: 'taxa',
    formBairro: 'UR-5'
},{
    nomeBairro: 'UR-6',
    valor: 7,
    taxa: 'taxa',
    formBairro: 'UR-6'
},{
    nomeBairro: 'UR-10',
    valor: 8,
    taxa: 'taxa',
    formBairro: 'UR-10'
},{
    nomeBairro: 'UR-11',
    valor: 8,
    taxa: 'taxa',
    formBairro: 'UR-11'
},{
    nomeBairro: 'UR-12',
    valor: 8,
    taxa: 'taxa',
    formBairro: 'UR-12'
},{
    nomeBairro: 'Jordão',
    valor: 8,
    taxa: 'taxa',
    formBairro: 'Jordão'
},{
    nomeBairro: 'Rio Xingu',
    valor: 7,
    taxa: 'taxa',
    formBairro: 'Rio Xingu'
},{
    nomeBairro: 'SESI',
    valor: 7,
    taxa: 'taxa',
    formBairro: 'SESI'
},{
    nomeBairro: 'Av. Recife',
    valor: 10,
    taxa: 'taxa',
    formBairro: 'Av. Recife'
},{
    nomeBairro: 'Monte Verde',
    valor: 8,
    taxa: 'taxa',
    formBairro: 'Monte Verde'
},{
    nomeBairro: 'Cavaleiro',
    valor: 10,
    taxa: 'taxa',
    formBairro: 'Cavaleiro'
},{
    nomeBairro: 'Ipsep',
    valor: 10,
    taxa: 'taxa',
    formBairro: 'Ipsep'
},{
    nomeBairro: 'Zumbi do Pacheco',
    valor: 8,
    taxa: 'taxa',
    formBairro: 'Zumbi do Pacheco'
},{
    nomeBairro: '2 Carneiros',
    valor: 7,
    taxa: 'taxa',
    formBairro: '2 Carneiros'
},{
    nomeBairro: 'Boa Viagem',
    valor: 15,
    taxa: 'taxa',
    formBairro: 'Boa Viagem'
},{
    nomeBairro: 'Piedade',
    valor: 15,
    taxa: 'taxa',
    formBairro: 'Piedade'
},{
    nomeBairro: 'Candeias',
    valor: 20,
    taxa: 'taxa',
    formBairro: 'Candeias'
},{
    nomeBairro: '3 Carneiros',
    valor: 7,
    taxa: 'taxa',
    formBairro: '3 Carneiros'
},{
    nomeBairro: 'Outros bairros (a taxa será informada no final)',
    valor: 0,
    taxa: 'taxa',
    formBairro: 'Outros bairros'
}]

const generateDomDistrict = (bairro) => {
    const bairroEl = document.createElement('tr')
    const inputH = document.createElement('input')
    const tdGeral = document.createElement('td')
    const tdBairro = document.createElement('td')
    const tdValor = document.createElement('td')
    const tdBtn = document.createElement('td')
    const btn = document.createElement('button')

    bairroEl.appendChild(tdGeral)
    tdGeral.appendChild(inputH)
    inputH.setAttribute('type', 'hidden')
    inputH.setAttribute('value', '1')
    tdGeral.setAttribute('style', 'display: none;')

    bairroEl.appendChild(tdBairro)
    tdBairro.textContent = bairro.nomeBairro

    bairroEl.appendChild(tdValor)
    tdValor.textContent = 'R$ ' + bairro.valor.toFixed(2).replace('.', ',')

    bairroEl.appendChild(tdBtn)
    btn.textContent = 'ESCOLHER'
    tdBtn.appendChild(btn)

    btn.addEventListener('click', (e) => {
        e.preventDefault()
        if(bairro.nomeBairro === 'Retirar na loja' || bairro.nomeBairro === 'Outros bairros (a taxa será informada no final)') {
            criaPedidos(parseFloat(inputH.value), bairro.nomeBairro, bairro.valor, bairro.taxa, bairro.formBairro)
        } else{
            criaPedidos(parseFloat(inputH.value), ('TAXA DE ENTREGA ' + bairro.nomeBairro), bairro.valor, bairro.taxa, bairro.formBairro)
        }

        location.assign('./confirma.html')
    })

    return bairroEl

}

const addBairrosTela = () => {
    bairrosLista.forEach((bairro) => {
        document.querySelector('table').appendChild(generateDomDistrict(bairro))
    })
}

export {addTblEl, addBairrosTela, generateDomDistrict, pulaTaxa}