const path = require('path')

module.exports = {
    entry: {
        index: ['babel-polyfill', './src/index.js'],
        cardapio: ['babel-polyfill', './src/cardapio.js'],
        combos: ['babel-polyfill', './src/combos.js'],
        conftemp: ['babel-polyfill', './src/conftemp.js'],
        promocoes: ['babel-polyfill', './src/promocoes.js'],
        beverage: ['babel-polyfill', './src/beverage.js'],
        confirma: ['babel-polyfill', './src/confirma.js'],
        txentrega: ['babel-polyfill', './src/txentrega.js'],
        forms: ['babel-polyfill', './src/forms.js'],
        confcli: ['babel-polyfill', './src/confcli.js'],
        enviapedidos: ['babel-polyfill', './src/enviapedidos.js'],
        closed: ['babel-polyfill', './src/closed.js']
    },
    output: {
        path: path.resolve(__dirname, 'public/scripts'),
        filename: '[name]-bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        publicPath: '/scripts/'
    },
    devtool: 'source-map'
}