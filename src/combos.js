import {apagaHams} from './hamburguer'
import {apagaRefris, apagaKids} from './refris'
import { addCombos } from './menuHam'

apagaHams()
apagaRefris()
apagaKids()
addCombos()

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})