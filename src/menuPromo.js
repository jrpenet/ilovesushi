import {criaPedidos} from './pedidos'
import {criatemp} from './temp'

const cardapioPromocoes = [{
    nomeHamburguer: 'Promoção - 2 Temakis Kani',
    descricao: '2 Temakis kani',
    preco: 18.9,
    foto: './images/clonetemakikani.jpg'
},{
    nomeHamburguer: 'Promoção - 40 Makimonos',
    descricao: 'Escolha entre os sabores: Salmão, Kani e Camarão',
    preco: 44.99,
    foto: './images/40makimonos.jpg'
},{
    nomeHamburguer: 'Promoção - 12 Peças de Uramaki',
    descricao: 'Escolha entre os sabores: Salmão, Kani, Camarão e Camarão Empanado',
    preco: 13.99,
    foto: './images/12uramaki1399.jpg'
},{
    nomeHamburguer: 'Promoção - 12 Peças de Dragon',
    descricao: 'Sabor Salmão',
    preco: 14.99,
    foto: './images/12dragon1499.jpg'
},{
    nomeHamburguer: 'Promoção - 30 Peças + 1 Temaki Salmão',
    descricao: '10 uramaki kani + 10 uramaki salmão + 10 carioca + 1 temaki salmão',
    preco: 44.99,
    foto: './images/30p.jpg'
},{
    nomeHamburguer: 'Promoção - 2 Temakis skin e 12 Cariocas de Salmão',
    descricao: '',
    preco: 29.99,
    foto: './images/gigante.jpg'
},{
    nomeHamburguer: 'Promoção - 24 Carioca de Salmão',
    descricao: '',
    preco: 19.99,
    foto: './images/P53ypALVKSSdhpmPYkDXDSsVfY4vfSa7rDBSEems.jpeg'
},{
    nomeHamburguer: 'Promoção - 12 Uramaki Salmão + 1 Temaki Salmão',
    descricao: '',
    preco: 35,
    foto: './images/uramakisalmaoetemaki.jpg'
},{
    nomeHamburguer: 'Promoção - 24 Carioca de Kani',
    descricao: '',
    preco: 16.9,
    foto: './images/1b40616a3e5dd0ba1d767765bdcdb04a.jpg'
},{
    nomeHamburguer: 'Promoção - 2 Temaki Kani Empanado Kani',
    descricao: '',
    preco: 19.9,
    foto: './images/images13jpeg.jpg'
},{
    nomeHamburguer: 'Temaki no Copo',
    descricao: 'Escolhas os sabores: Salmão, camarão, kani e misto',
    preco: 17.99,
    foto: './images/temaki no copo.jpg'
}]

const addElementsPromocoes = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer descricaoBebida')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tipoEl = lanche.tipo

        if(prodt === 'Clone de Temakis - Frito' || prodt === 'Clone de Temakis' || prodt === 'Temaki no Copo' || prodt === 'Promoção - 40 Makimonos' || prodt === 'Promoção - 12 Peças de Uramaki') {
            criatemp(qt, prodt, price)
            location.assign('./conftemp.html')
            //alert('Clone de temaki')
        }else{
            criaPedidos(qt, prodt, price, tipoEl)
            location.assign('./beverage.html') 
            //alert('outros')
        }
               
    })

    imagem.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tipoEl = lanche.tipo

        if(prodt === 'Clone de Temakis - Frito' || prodt === 'Clone de Temakis' || prodt === 'Temaki no Copo' || prodt === 'Promoção - 40 Makimonos' || prodt === 'Promoção - 12 Peças de Uramaki') {
            criatemp(qt, prodt, price)
            location.assign('./conftemp.html')
            //alert('Clone de temaki')
        }else{
            criaPedidos(qt, prodt, price, tipoEl)
            location.assign('./beverage.html') 
            //alert('outros')
        }
               
    })

    return divMain
}

const addMenuPromocoes = () => {
    cardapioPromocoes.forEach((lanche) => {
        const linhaHoriz = document.createElement('hr')
        document.querySelector('#docPromocoes').appendChild(linhaHoriz)
        document.querySelector('#docPromocoes').appendChild(addElementsPromocoes(lanche))
    })
}

export {addMenuPromocoes}