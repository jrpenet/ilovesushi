import {gettemp, apagatemp} from './temp'
import {criaPedidos} from './pedidos'
import { addCardapioMolho } from './menuHam'
//import { addCardapioMolho, addCardapioMolhoEspetinho, addCardapioRecheios } from './molhosrecheios'

if(gettemp().length > 0 && gettemp()[0].produto !== 'Temakis'){
    addCardapioMolho()
}

//sabores: 0 skin, 1 camarao, 2 kani, 3 salmao, 4 misto, 5 polvo

if(gettemp()[0].produto == 'Temakis'){
    //remove display none da seção
    const showtmk = document.querySelector('#secaoTEMAKI')
    const showtmkE = document.querySelector('#secaoTEMAKIE')
    showtmk.removeAttribute('style', 'display: none;')
    showtmkE.removeAttribute('style', 'display: none;')
}

if(gettemp()[0].produto === 'Clone de Temakis - Frito' || gettemp()[0].produto === 'Clone de Temakis'){
    //sabores: 0 skin, 1 camarao, 2 kani, 3 salmao, 4 misto, 5 polvo

    let elementoA = document.querySelector('#molhosRecheios').children[1]
    let elementoB = document.querySelector('#molhosRecheios').children[3] // .container #chkMolhosalmao
    let elementoC = document.querySelector('#molhosRecheios').children[4]
    let elementoD = document.querySelector('#molhosRecheios').children[5]
    elementoA.setAttribute('style', 'display: none;')
    elementoB.setAttribute('style', 'display: none;')  
    elementoC.setAttribute('style', 'display: none;')  
    elementoD.setAttribute('style', 'display: none;')  
}

if(gettemp()[0].produto === 'Uramaki Filadelfia'){
    //sabores: 0 skin, 1 camarao, 2 kani, 3 salmao, 4 misto, 5 polvo

    let elementoA = document.querySelector('#molhosRecheios').children[4]
    let elementoD = document.querySelector('#molhosRecheios').children[5]
    elementoA.setAttribute('style', 'display: none;')
    elementoD.setAttribute('style', 'display: none;')  
}

if(gettemp()[0].produto === 'Sushi Hot - Ilovesushi' || gettemp()[0].produto === 'Temaki no Copo' || gettemp()[0].produto === 'Sunomono'){
    //sabores: 0 skin, 1 camarao, 2 kani, 3 salmao, 4 misto, 5 polvo

    let elementoA = document.querySelector('#molhosRecheios').children[0]
    let elementoD = document.querySelector('#molhosRecheios').children[5]
    elementoA.setAttribute('style', 'display: none;')
    elementoD.setAttribute('style', 'display: none;')  
}

if(gettemp()[0].produto === 'Sashimi (sashimi unidade)'){
    //sabores: 0 skin, 1 camarao, 2 kani, 3 salmao, 4 misto, 5 polvo

    let elementoA = document.querySelector('#molhosRecheios').children[0]
    let elementoC = document.querySelector('#molhosRecheios').children[4]
    elementoA.setAttribute('style', 'display: none;')
    elementoC.setAttribute('style', 'display: none;')  
}

if(gettemp()[0].produto === 'Hossomaki' || gettemp()[0].produto === 'Promoção - 40 Makimonos' || gettemp()[0].produto === 'Promoção - 12 Peças de Uramaki'){
    //sabores: 0 skin, 1 camarao, 2 kani, 3 salmao, 4 misto, 5 polvo

    let elementoA = document.querySelector('#molhosRecheios').children[0]
    let elementoB = document.querySelector('#molhosRecheios').children[4] // .container #chkMolhosalmao
    let elementoC = document.querySelector('#molhosRecheios').children[5]
    
    elementoA.setAttribute('style', 'display: none;')
    elementoB.setAttribute('style', 'display: none;')  
    elementoC.setAttribute('style', 'display: none;')  
}

//mostra o nome do pedido na pagina
const mostraNomeDoPedido = document.querySelector('#nomePedido')
mostraNomeDoPedido.textContent = gettemp()[0].produto
//pega o q foi digitado em obs
//const pegaObs = document.querySelector('#insereObs')

//botao que esta na page
const botaoConfirmaInsere = document.querySelector('#confirmaInsere')

botaoConfirmaInsere.addEventListener('click', (e) => {
    e.preventDefault()
    if(gettemp().length > 0 && gettemp()[0].produto !== 'Temakis'){
        //pega o q foi selecionado no radiobox
        const inforadio1 = document.getElementById('chkMolhoskin')
        const inforadio2 = document.getElementById('chkMolhocamarao')
        const inforadio3 = document.getElementById('chkMolhokani')
        const inforadio4 = document.getElementById('chkMolhosalmao')
        const inforadio5 = document.getElementById('chkMolhomisto')
        const inforadio6 = document.getElementById('chkMolhopolvo')

        if(inforadio1.checked == true){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor: ' + inforadio1.value, gettemp()[0].preco)
            apagatemp()
        }
        if(inforadio2.checked == true){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor: ' + inforadio2.value, gettemp()[0].preco)
            apagatemp()
        }
        if(inforadio3.checked == true){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor: ' + inforadio3.value, gettemp()[0].preco)
            apagatemp() 
        }
        if(inforadio4.checked == true){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor: ' + inforadio4.value, gettemp()[0].preco)
            apagatemp()
        }
        if(inforadio5.checked == true){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor: ' + inforadio5.value, gettemp()[0].preco)
            apagatemp()
        }

        if(inforadio6.checked == true){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor: ' + inforadio6.value, gettemp()[0].preco)
            apagatemp()
        }
        
        location.assign('./confirma.html')
    }

    if(gettemp()[0].produto == 'Temakis'){
        let boxes = document.getElementsByName('saborTMKNormal')
        let rs = ''

        for(let i = 0; i < boxes.length; i++){
            if(boxes[i].checked === true){
                rs += boxes[i].value
            }
        }

        if(rs == 'Salmão' || rs == 'Camarão'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 18.9)
            apagatemp()
        }
        if(rs == 'Kani'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 16.9)
            apagatemp()
        }
        if(rs == 'Misto'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 16.99)
            apagatemp()
        }
        if(rs == 'Skin' || rs == 'Carioca'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 13.99)
            apagatemp()
        }
        //empanados
        if(rs == 'Salmão Empanado' || rs == 'Camarão Empanado'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 20.4)
            apagatemp()
        }
        if(rs == 'Kani Empanado'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 18.4)
            apagatemp()
        }
        if(rs == 'Misto Empanado'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 18.49)
            apagatemp()
        }
        if(rs == 'Skin Empanado' || rs == 'Carioca Empanado'){
            criaPedidos(gettemp()[0].qtd, 'Temaki' + ' Sabor: ' + rs, 15.49)
            apagatemp()
        }

        location.assign('./confirma.html')
    }
    
})